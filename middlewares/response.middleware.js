const responseMiddleware = (req, res, next) => {
    //TODO: Implement middleware that returns result of the query
    if(res.err =='All fields are not filled') {
        return res.status(400).json({
            error: true,
            message: 'All fields are not filled' 
        });
    }
    if(res.err == 'Email not valid') {
        return res.status(400).json({
            error: true,
            message: 'Email is not valid' 
        });
    }
    if(res.err == `User with email this already exists`) {
        return res.status(400).json({
            error: true,
            message: `User with email this already exists` 
        });
    }
    if(res.err == 'Phone number not valid') {
        return res.status(400).json({
            error: true,
            message: 'Phone number is not valid'
        });
    }
    if(res.err == `User with phone this already exists`) {
        return res.status(400).json({
            error: true,
            message: `User with phone this already exists`
        });
    }
    if(res.err == 'Password too short') {
        return res.status(400).json({
            error: true,
            message: 'Password too short' 
        });
    }
    if(res.err == 'Users not found') {
        return res.status(404).json({
            error: true,
            message: 'Users not found' 
        });
    }

    if(res.err == 'User not found') {
        return res.status(404).json({
            error: true,
            message: 'User not found' 
        });
    }
    if(res.err == 'String is empty') {
        return res.status(400).json({
            error: true,
            message: 'String is empty' 
        });
    }

    if(res.err == 'Fighters not found') {
        return res.status(404).json({
            error: true,
            message: 'Users not found' 
        });
    }

    if(res.err == 'Fighter not found') {
        return res.status(404).json({
            error: true,
            message: 'User not found' 
        });
    }

    if(res.err == 'Fighter with name this already exists') {
        return res.status(400).json({
            error: true,
            message: 'Fighter with name this already exists' 
        });
    }

    if(res.err == 'Health must be between 80 and 120') {
        return res.status(400).json({
            error: true,
            message: 'Health must be between 80 and 120' 
        });
    }
    if(res.err == 'Power must be between 1 and 100') {
        return res.status(400).json({
            error: true,
            message: 'Power must be between 1 and 100' 
        });
    }
    if(res.err == 'Defense must be between 1 and 10') {
        return res.status(400).json({
            error: true,
            message: 'Defense must be between 1 and 10' 
        });
    }
    if(res.err == 'Id in request body') {
        return res.status(400).json({
            error: true,
            message: 'Id in request body' 
        });
    }

    if(res.err == 'Health not a number') {
        return res.status(400).json({
            error: true,
            message: 'Health not a number' 
        });
    }
    if(!res.err) {
        res.status(200).send(res.data);
    }
    next();
}

exports.responseMiddleware = responseMiddleware;