const { fighter } = require('../models/fighter');

const createFighterValid = (req, res, next) => {
    // TODO: Implement validatior for fighter entity during creation

    const {
        name,
        health,
        power,
        defense
    } = req.body
  
    const fighter = {
        name,
        health,
        power,
        defense
    }

    if (req.body.hasOwnProperty('id')) {
        res.err = 'Id in request body'
    }

    for (const itemsFromBodyIndex in fighter){
        if (fighter.hasOwnProperty(itemsFromBodyIndex)) {

            if(fighter[itemsFromBodyIndex] == null){
                res.err = 'All fields are not filled'     
            }
        }
    }

    if (typeof(health) !== "number") {
        res.err = 'Health not a number'
    }

    if (power < 1) {
        res.err = 'Power must be between 1 and 100'
    }
    if (power > 100) {
        res.err = 'Power must be between 1 and 100'
    }

    if (defense < 1) {
        res.err = 'Defense must be between 1 and 10'
    }
    if (defense > 10) {
        res.err = 'Defense must be between 1 and 10'
    }

    if (health < 80) {
        res.err = 'Health must be between 80 and 120'        
    }
    if (health > 120) {
        res.err = 'Health must be between 80 and 120'        
    }

    next();
}

const updateFighterValid = (req, res, next) => {
    // TODO: Implement validatior for fighter entity during update

    const {
        name,
        health,
        power,
        defense
    } = req.body
  
    const fighter = {
        name,
        health,
        power,
        defense
    }

    if (req.body.hasOwnProperty('id')) {
        res.err = 'Id in request body'
    }

    for (const itemsFromBodyIndex in fighter){
        if (fighter.hasOwnProperty(itemsFromBodyIndex)) {

            if(fighter[itemsFromBodyIndex] == null){
                res.err = 'All fields are not filled'     
            }
        }
    }

    next();
}

exports.createFighterValid = createFighterValid;
exports.updateFighterValid = updateFighterValid;