const { user } = require('../models/user');
const createUserValid = (req, res, next) => {
    // TODO: Implement validatior for user entity during creation

    const {
        firstName,
        lastName,
        email,
        phoneNumber,
        password
    } = req.body

    const dataFields = {
        firstName,
        lastName,
        email,
        phoneNumber,
        password
    }

    if (req.body.hasOwnProperty('id')) {
        res.err = 'Id in request body'
    }

    for (const itemsFromBodyIndex in dataFields){
        if (dataFields.hasOwnProperty(itemsFromBodyIndex)) {
    
            if(dataFields[itemsFromBodyIndex] == null){
                res.err = 'All fields are not filled'     
            }
        }
    }

    const regEmeil = /([a-zA-Z0-9]+)([\.{1}])?([a-zA-Z0-9]+)\@gmail([\.])com/g;

    if (!regEmeil.test(String(email).toLowerCase())) {
        res.err = 'Email not valid'
    }

    const regPhone = /^\+380\d{3}\d{2}\d{2}\d{2}$/;

    if (!regPhone.test(String(phoneNumber))) {
        res.err = 'Phone number not valid'
    }
  
    if (String(password).length < 3) {
        res.err = 'Password too short'
    }
    next();
}

const updateUserValid = (req, res, next) => {
    // TODO: Implement validatior for user entity during update

    const {
        firstName,
        lastName,
        email,
        phoneNumber,
        password
    } = req.body

    if (req.body.hasOwnProperty('id')) {
        res.err = 'Id in request body'
    }

    const regEmeil = /([a-zA-Z0-9]+)([\.{1}])?([a-zA-Z0-9]+)\@gmail([\.])com/g;

    if (!regEmeil.test(String(email).toLowerCase())) {
        res.err = 'Email not valid'
    }

    const regPhone = /^\+380\d{3}\d{2}\d{2}\d{2}$/;

    if (!regPhone.test(String(phoneNumber))) {
        res.err = 'Phone number not valid'
    }
  
    if (String(password).length < 3) {
        res.err = 'Password too short'
    }

    next();
}

exports.createUserValid = createUserValid;
exports.updateUserValid = updateUserValid;