const { Router } = require('express');
const UserService = require('../services/userService');
const { createUserValid, updateUserValid } = require('../middlewares/user.validation.middleware');
const { responseMiddleware } = require('../middlewares/response.middleware');

const router = Router();

// TODO: Implement route controllers for user

router.get('/', (req, res, next) => {
  try {
    const data = UserService.searchAll()

    data.length == 0 ? res.err = 'Users not found' : res.data = data;

  } catch (err) {
    res.err = err;
  } finally {
    next();
  }
}, responseMiddleware);

router.get('/:id', (req, res, next) => {
  try {
    const id = req.params.id;
    const data = UserService.search({"id": id})

    data == null ? res.err = 'User not found' : res.data = data;

  } catch (err) {
    res.err = err;
  } finally {
    next();
  }
}, responseMiddleware);

router.delete('/:id', (req, res, next) => {
  try {
    const id = req.params.id;
    const data = UserService.delete(`${id}`)

    data.length == 0 ? res.err = 'User not found' : res.data = data;

  } catch (err) {
    res.err = err;
  } finally {
    next();
  }
}, responseMiddleware);

router.post('/', createUserValid, (req, res, next) => {
  try {
    const {
      firstName,
      lastName,
      email,
      phoneNumber,
      password
    } = req.body

    const candidateEmail = UserService.search({"email": email})

    if (candidateEmail) {
      res.err = `User with email this already exists`
    }

    const candidatePhone = UserService.search({"phoneNumber": phoneNumber})

    if (candidatePhone) {
      res.err = `User with phone this already exists`
    }

    const user = {
      firstName,
      lastName,
      email,
      phoneNumber,
      password
    }

    let data = null;
   
    if (!res.err) {
      data = UserService.create(user)
      res.data = data
    }

  } catch (err) { 
     res.err = err;
  } finally {
    next(); 
  }
}, responseMiddleware);

router.put('/:id', updateUserValid, (req, res, next) => {
  try {
    const id = req.params.id;

    const {
      lastName,
      email,
      phoneNumber,
      password,
      firstName,
    } = req.body

    const candidateEmail = UserService.search({"email": email})

    if (candidateEmail) {
      res.err = `User with email this already exists`
    }

    const candidatePhone = UserService.search({"phoneNumber": phoneNumber})

    if (candidatePhone) {
      res.err = `User with phone this already exists`
    }

    const user = {
      firstName,
      lastName,
      email,
      phoneNumber,
      password
    }

    if (!res.err) {
      data = UserService.update(`${id}`, user)
      res.data = data
    }
    
  } catch (err) {
    res.err = err;
  } finally {
    next();
  }
}, responseMiddleware);

module.exports = router;