const { Router } = require('express');
const FighterService = require('../services/fighterService');
const { responseMiddleware } = require('../middlewares/response.middleware');
const { createFighterValid, updateFighterValid } = require('../middlewares/fighter.validation.middleware');

const router = Router();

// TODO: Implement route controllers for fighter

router.get('/', (req, res, next) => {
  try {
    const data = FighterService.searchAll()

    data.length == 0 ? res.err = 'Fighter not found' : res.data = data;

  } catch (err) {
    res.err = err;
  } finally {
    next();
  }
}, responseMiddleware);

router.get('/:id', (req, res, next) => {
  try {
    const id = req.params.id;
    const data = FighterService.search({"id": id})

    data == null ? res.err = 'Fighter not found' : res.data = data;

  } catch (err) {
    res.err = err;
  } finally {
    next();
  }
}, responseMiddleware);

router.delete('/:id', (req, res, next) => {
  try {
    const id = req.params.id;
    const data = FighterService.delete(`${id}`)

    data.length == 0 ? res.err = 'Fighter not found' : res.data = data;

  } catch (err) {
    res.err = err;
  } finally {
    next();
  }
}, responseMiddleware);

router.post('/', createFighterValid, (req, res, next) => {
  try {
    const {
      name,
      health,
      power,
      defense
    } = req.body

    const candidateName = FighterService.search({"name": name})

    if (candidateName) {
      res.err = `Fighter with name this already exists`
    }

    const fighter = {
      name,
      health,
      power,
      defense
    }

    let data = null;
   
    if (!res.err) {
      data = FighterService.create(fighter)
      res.data = data
    }

  } catch (err) { 
     res.err = err;
  } finally {
    next(); 
  }
}, responseMiddleware);

router.put('/:id', updateFighterValid, (req, res, next) => {
  try {
    const id = req.params.id;

    const {
      lastName,
      email,
      phoneNumber,
      password,
      firstName,
    } = req.body

    const fighter = {
      firstName,
      lastName,
      email,
      phoneNumber,
      password
    }

    let data = null;
   
    if (!res.err) {
      data = FighterService.update(`${id}`, fighter)
      res.data = data
    }
  } catch (err) {
    res.err = err;
  } finally {
    next();
  }
}, responseMiddleware);

module.exports = router;