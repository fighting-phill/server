const { FighterRepository } = require('../repositories/fighterRepository');

class FighterService {
    // TODO: Implement methods to work with fighters
    searchAll(all) {
        const items = FighterRepository.getAll();
        
        if(!items) {
            return null;
        }
        return items;
    }

    search(search) {
        const item = FighterRepository.getOne(search);
        if(!item) {
            return null;
        }
        return item;
    }

    create(data) {
        const item = FighterRepository.create(data);
        if(!item) {
            return null;
        }
        return item;
    }

    update(id, data) {
        const item = FighterRepository.update(id, data);
        if(!item) {
            return null;
        }
        return item;
    }

    delete(idToDelete) {
        const item = FighterRepository.delete(idToDelete);
        if(!item) {
            return null;
        }
        return item;
    }
}

module.exports = new FighterService();